
# Create your views here.

from django.http import HttpResponse
from django.views.generic import CreateView

from .models import Libro, Autor, Genero, Critica, CustomUser
from django.db.models import Avg

def index(request):
    """
    Función vista para la página inicio del sitio.
    """
    if request.user.is_authenticated:
        li= Libro.objects.all().order_by('-id')[:3]
    # Renderiza la plantilla HTML index.html con los datos en la variable contexto
        return render(
        request,
        'index.html',
        context={'li': li, },
        )
    else:
        return redirect('login')



def genero(request):
    """
    Función vista para la página inicio del sitio.
    """
    # Generate counts of some of the main objects
    p = Genero.objects.all()
    # Renderiza la plantilla HTML index.html con los datos en la variable contexto
    return render(
        request,
        'genero.html',
        context={'p': p, },

    )

def muestralibros(request, genero_id):
    gen = Genero.objects.get(pk=genero_id)
    li = Libro.objects.filter(genero__name__startswith=gen)

    # Renderiza la plantilla HTML muestralibros.html con los datos en la variable contexto
    return render(
        request,
        'muestralibros.html',
        context={'li': li, 'gen': gen, },

    )


def ranking(request):


    puntua = sorted(Libro.objects.all(), key=lambda libro: -libro.media)
    return render(
        request,
        'ranking.html',
        context={'puntua': puntua}

    )

def muestracomentarios(request, libro_id):

    coment = Libro.objects.get(pk=libro_id)
    critica = Critica.objects.filter(titulo_id=coment)


    # Renderiza la plantilla HTML index.html con los datos en la variable contexto
    return render(
        request,
        'muestracomentarios.html',
        context={'coment' :coment, 'critica': critica}

    )

def sugerencia(request):
    # Renderiza la plantilla HTML index.html con los datos en la variable contexto
    user_id = request.user.pk
    if user_id:

        sug = CustomUser.objects.get(pk= user_id)
        genero = sug.categoria
        librosug = Libro.objects.filter(genero=genero)
        return render(
        request,
        'sugerencia.html',
        context={'sug': sug,'genero':genero, 'librosug':librosug},
        )
    else:
        return render(
            request,
            'error_sugerencia.html',

        )


from django.shortcuts import render, redirect

from django.urls import reverse_lazy
from django.views import generic

from .forms import CustomUserCreationForm, FormCrearCritica, FormCrearLibro, FormCrearAutor


class SignUp(generic.CreateView):
    form_class = CustomUserCreationForm
    success_url = reverse_lazy('login')
    template_name = 'signup.html'


class CriticaCreateView(CreateView):
    model = Libro
    # Caracteristicas especiales para el form de crear
    success_url = reverse_lazy('criticadone')
    form_class = FormCrearCritica
    template_name = 'critica.html'

class NuevoLibroView(CreateView):
    model = Libro
    # Caracteristicas especiales para el form de crear
    success_url = reverse_lazy('librodone')
    form_class = FormCrearLibro
    template_name = 'nuevolibro.html'

class NuevoAutorView(CreateView):
    model = Autor
    # Caracteristicas especiales para el form de crear
    success_url = reverse_lazy('nuevolibro')
    form_class = FormCrearAutor
    template_name = 'nuevoautor.html'

def criticadone(request):

    # Renderiza la plantilla HTML criticadone.html con los datos en la variable contexto
    return render(
        request,
        'criticadone.html',

    )
def librodone(request):

    # Renderiza la plantilla HTML librodone.html con los datos en la variable contexto
    return render(
        request,
        'librodone.html',

    )
