
from django.urls import path
from django.conf import settings
from django.conf.urls.static import static
from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('signup/', views.SignUp.as_view(), name='signup'),
    path('genero/', views.genero, name='genero'),
    path('critica/', views.CriticaCreateView.as_view(), name='critica'),
    path('critica/criticadone/', views.criticadone, name='criticadone'),
    path('ranking/', views.ranking, name='ranking'),
    path('genero/<slug:genero_id>/', views.muestralibros, name='muestralibros'),
    path('ranking/<slug:libro_id>/', views.muestracomentarios, name='muestracomentarios'),
    path('nuevolibro/', views.NuevoLibroView.as_view(), name='nuevolibro'),
    path('nuevolibro/librodone/', views.librodone, name='librodone'),
    path('nuevolibro/nuevoautor/', views.NuevoAutorView.as_view(), name='nuevoautor'),
    path('sugerencia/', views.sugerencia, name='sugerencia'),
    path('error_sugerencia/', views.sugerencia, name='error_sugerencia'),
] + static(settings.MEDIA_URL,document_root=settings.MEDIA_ROOT)



