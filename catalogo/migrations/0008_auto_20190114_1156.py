# Generated by Django 2.1.3 on 2019-01-14 10:56

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('catalogo', '0007_auto_20190114_1029'),
    ]

    operations = [
        migrations.AlterField(
            model_name='critica',
            name='usuario',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to=settings.AUTH_USER_MODEL),
        ),
    ]
