# Generated by Django 2.1.3 on 2018-12-03 09:50

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('catalogo', '0002_critica'),
    ]

    operations = [
        migrations.AddField(
            model_name='critica',
            name='titulo',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='catalogo.Libro'),
        ),
    ]
