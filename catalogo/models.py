# Create your models here.

from django.contrib.auth.models import AbstractUser
from django.db import models
from django.db.models import Avg



class CustomUser(AbstractUser):
    categoria = models.ForeignKey('Genero', on_delete=models.SET_NULL, null=True)

    def __str__(self):
        return '{0}'.format(self.username)


class Genero(models.Model):
    """
    Modelo que representa un género literario (
    """
    name = models.CharField(max_length=100, help_text="Introduce género del libro")
    imagen = models.ImageField(
        upload_to='portadas/')

    def __str__(self):
        """
        Cadena que representa a la instancia particular del modelo (p. ej en el sitio de Administración)
        """
        return self.name


import reverse  # Used to generate URLs by reversing the URL patterns


class Libro(models.Model):
    """
    Modelo que representa un libro (pero no un Ejemplar específico).
    """
    titulo = models.CharField(max_length=200)

    autor = models.ForeignKey('Autor', on_delete=models.SET_NULL, null=True)
    # ForeignKey, ya que un libro tiene un solo autor, pero el mismo autor puede haber escrito muchos libros.
    # 'Author' es un string, en vez de un objeto, porque la clase Author aún no ha sido declarada.

    sinopsis = models.TextField(max_length=1000, help_text="Introduce una breve introducción del libro")

    genero = models.ManyToManyField(Genero, help_text="Selecciona el género del libro")

    portada = models.ImageField(
        upload_to='portadas/')  # Crea una carpeta llamada portadas, donde guardara las imagenes de portadas de libros, al final la imagen tendra que cargarse en: media/portadas/


    @property
    def media(self):
        avg = self.critica_set.aggregate(Avg('puntuacion'))
        if not avg['puntuacion__avg']:
            return 0.0
        else:
            return avg['puntuacion__avg']

    def __str__(self):
        """
        String que representa al objeto Book
        """
        return self.titulo

    def get_absolute_url(self):
        """
        Devuelve el URL a una instancia particular de Book
        """
        return reverse('librodone', args=[str(self.id)])


class Autor(models.Model):
    """Model representing an author."""
    nombre = models.CharField(max_length=100)
    apellido = models.CharField(max_length=100)

    class Meta:
        ordering = ['apellido', 'nombre']

    def get_absolute_url(self):
        """Returns the url to access a particular author instance."""
        return reverse('author-detail', args=[str(self.id)])

    def __str__(self):
        """String for representing the Model object."""
        return '{0}, {1}'.format(self.apellido, self.nombre)


from django.core.validators import MaxValueValidator


class Critica(models.Model):
    """Modelo Critica."""

    titulo = models.ForeignKey('Libro', on_delete=models.SET_NULL, null=True)
    puntuacion = models.PositiveIntegerField(validators=[MaxValueValidator(10)],
                                             help_text="Introduce una puntuación del 1 al 10")
    comentario = models.TextField(max_length=1000)
    usuario = models.ForeignKey('CustomUser', on_delete=models.SET_NULL, null=True)

    class Meta:
        ordering = ['usuario', 'puntuacion']

    def get_absolute_url(self):
        """Returns the url """
        return reverse('criticadone', args=[str(self.id)])

    def __str__(self):
        """String for representing the Model object."""
        return '{0}, {1}'.format(self.titulo, self.puntuacion)
