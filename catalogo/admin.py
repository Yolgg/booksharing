from django.contrib import admin
from django.contrib.auth import get_user_model
from django.contrib.auth.admin import UserAdmin

from .forms import CustomUserCreationForm, CustomUserChangeForm
from .models import CustomUser
# Register your models here.
from .models import Autor, Genero, Libro, Critica

admin.site.register(Libro)
admin.site.register(Autor)
admin.site.register(Genero)
admin.site.register(Critica)


class CustomUserAdmin(UserAdmin):
    add_form = CustomUserCreationForm
    form = CustomUserChangeForm
    model = CustomUser

    fieldsets = (
        (('User'), {'fields': ('username', 'email', 'is_staff', 'categoria')}),

    )

admin.site.register(CustomUser, CustomUserAdmin)

