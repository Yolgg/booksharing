from django import forms
from django.contrib.auth.forms import UserCreationForm, UserChangeForm
from django.forms import ModelForm

from .models import CustomUser, Critica, Libro, Autor


class CustomUserCreationForm(UserCreationForm):
    class Meta(UserCreationForm):
        model = CustomUser
        fields = ('username', 'email', 'categoria')


class CustomUserChangeForm(UserChangeForm):
    class Meta:
        model = CustomUser
        fields = ('username', 'email')


class FormCrearCritica(forms.ModelForm):
    class Meta:
        model = Critica
        fields = ('titulo', 'puntuacion', 'comentario', 'usuario')


class FormCrearLibro(forms.ModelForm):
    class Meta:
        model = Libro
        fields = ('titulo', 'autor', 'sinopsis', 'genero', 'portada')


class FormCrearAutor(forms.ModelForm):
    class Meta:
        model = Autor
        fields = ('nombre', 'apellido')
